# tex

Trainer for entering formulas in TeX syntax

This repository is an optional module for the "lerntools".

**This repository is not maintained. Please use https://codeberg.org/tk100/MintApps/src/branch/main/src/mint/views/TexTrainer.vue instead, which is compatible.**
