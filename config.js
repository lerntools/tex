
import Icon from './img/tex.svg';

export default {
	id: "tex",
	meta: {
		title: 	{
			"de": "TeX-Formel-Trainer",
			"en": "TeX formula trainer"
		},
		text:	{
			"de": "Trainer zur Eingabe von Formeln im TeX-Syntax",
			"en": "Trainer for entering formulas in TeX syntax"
		},
		to: 	"tex-trainer",
		icon: 	Icon,
		index: true
	},
	routes: [
		{	path: '/tex-trainer', name:'tex-trainer', component: () => import('./views/TexTrainer.vue') },
	]
}
